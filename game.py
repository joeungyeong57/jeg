# 간단한 텍스트 시나리오 게임을 만드시오
name = input("이름을 입력하시오: ")
print(name, "님 반갑습니다. {0}(HP 100)으로 게임을 시작 합니다.".format(name))
print("길을 가다가 퉁퉁이를 만났습니다.")
print("퉁퉁이와 가위바위보를 시작합니다.")

from random import randint

for i in range(1,5):
    print("1.가위 2.바위 3.보")

    computer = randint(1,3)
    user = int(input("선택: "))
    
    if user == 1:
        if computer == 1:
            print(name, "가위, 퉁퉁이: 가위")
            print("비겼다!!")
        elif computer == 2:
            print(name, "가위, 퉁퉁이: 바위")
            print("퉁퉁이에게 졌다!!")
        else:
            print(name, "가위, 퉁퉁이: 보")
            print("퉁퉁이에게 이겼다!!")
    
    elif user == 2:
        if computer == 1:
            print(name, "바위, 퉁퉁이: 가위")
            print("퉁퉁이에게 이겼다!!")
        elif computer == 2:
            print(name, "바위, 퉁퉁이: 바위")
            print("비겼다!!")
        else:
            print(name, "바위, 퉁퉁이: 보")
            print("퉁퉁이에게 졌다!!")

    else:
        if computer == 1:
            print(name, "보, 퉁퉁이: 가위")
            print("퉁퉁이에게 이겼다!!")
        elif computer == 2:
            print(name, "보, 퉁퉁이: 바위")
            print("퉁퉁이에게 졌다!!")
        else:
            print(name, "보, 퉁퉁이: 보")
            print("비겼다!!")